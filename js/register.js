function saveUserData() {
    var name, email, password, confirmPassword;
    name = document.getElementById("txtId").value;
    console.log("Saving the name: " + name);
    localStorage.setItem("Name", name);

    email = document.getElementById("txtEmail").value;
    console.log("Saving the email: " + email);
    localStorage.setItem("Email", email);

    password = document.getElementById("txtPassword").value;
    console.log("Saving the password: " + password);
    localStorage.setItem("Password", password);

    confirmPassword = document.getElementById("txtRepeatPassword").value;
    console.log("Saving the confirmation password: " + confirmPassword);
    localStorage.setItem("ConfirmPassword", confirmPassword);

};